==============================
tue-sfc-auto Documentation
==============================

**Disclaimer**: Demo software. Please use at your own full responsibility!


Introduction
=============

Available slot(s) for TU/e language courses is opened for registration at random time. This automated program does the following.

- Check if a slot is not full.
- Register into a group while avoiding the late (17:30-19:15) timeslot whenever possible.
- (linux only) A crontab script for repetitive execution.


Requirements
=============

- python >= 3.6
- python packages: selenium, urllib
- Using anaconda3 python is preferred but not necessary.


Usage
===========

.. code-block:: bash

    # 1. Download
    $ git clone https://gitlab.com/cbhuang/tue-sfc-auto
    $ cd tue-sfc-auto
    
    # 2. Install dependencies (assuming anaconda python)
    $ conda install selenium urllib
    # if you do not use anaconda python, use ``pip install``.
    
    # 3. Edit osiris.py
    # 3-1. Change parameters in __init__()
    # 3-2. Check s40_select_quarter() because it varies
    
    # 4. Edit your login information in login.json
    $ cp login.json.template login.json
    
    # 5. Run directly
    $ rm state.txt
    $ python osiris.py
    
    # 6. (Linux only) Run periodically using crontab
    # 6-1. Edit ``cronjob.sh`` to match your anaconda installation
    #      e.g. if you are using system python, conda activate is not necessary.    
    # 6-2. add something like this into system crontab
    #      0,20,40 * * * *  bill  cd /mnt/data/GitLab/tue-sfc-auto && bash cronjob.sh
    # This will execute every 20 minutes.
    # N.B. ``crontab -e`` won't work! Don't know why...
    $ sudo nano /etc/crontab 
    
