#!/bin/bash

# The following 2 lines can be removed is anaconda python is not used
source /opt/anaconda3/etc/profile.d/conda.sh
conda activate base

base_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$base_dir" || exit 1
python osiris.py
