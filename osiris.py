"""A Selenium-based web scraper for automated registration for TU/e language center courses.

:moduleauthor: Chuan-Bin "Bill" Huang
:date: 2019-11-27

WARNING:
    - Page structure may change over time. USE AT YOUR OWN RISK!
    - Check ``idx`` in ``self.s40_select_quarter`` before run!
"""

from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import WebDriverException
from urllib.parse import unquote
import time
from pathlib import Path
import json
from datetime import datetime
import os
import re

class OA:
    """Osiris-Auto main class."""
    
    def __init__(self):
        #: pathlib.Path: current working directory
        self.base_dir = Path(__file__).parent.absolute()
        #: str: registration status
        self.state_file = self.base_dir / "state.txt"
        #: str: login information (json format)
        self.login_file = self.base_dir / "login.json"
        #: str: program log
        self.log_file = self.base_dir / "osiris.log"
        #: io.TextIOWrapper: file pointer of the opened log file
        self.fp_log = open(self.log_file, "a")
        #: bool: driver options
        self.hide_window = True
        #: selenium.webdriver.Firefox: backend browser
        self.driver = self.init_driver()
        #: str: course code
        self.course_code = "SFC530"
        #: int: target quarter
        self.q = 4
        #: int: current quarter (affects ``self.s40_select_quarter()``!)
        self.q_now = 2
        #: bool: cleanup driver logfile
        self.del_geckodriver = True
        #: dict: available block and starting hour
        self.dic_avail = {}

    def master(self):
        """Master function"""
        self.s10_check_connectivity()
        self.s20_login()
        self.s30_query_course()
        self.s40_select_quarter()
        self.s50_check_available_groups()
        self.s60_register_preferred()
        self.s90_logout()

    def s10_check_connectivity(self):
        """Proceed to login page"""
        
        # connect the site
        try:
            self.driver.get("https://osiris.tue.nl")
            self.fp_log.write(f"{self.lp()} Connection established...\n")
            time.sleep(3)
        except WebDriverException as e:
            self.fp_log.write(f"{self.lp('ERROR')} Connection problem: {unquote(str(e))}\n")
            self.exit_program(1)
                
        # bypass occasional announcement page
        try:
            self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/table/tbody/tr[2]/td[3]/button").click()
            self.fp_log.write(f"{self.lp()} Announcement page bypassed...\n")
            time.sleep(3)
        except WebDriverException as e:
            self.fp_log.write(f"{self.lp()} Announcement page does not exist...\n")

    def s20_login(self):
        
        # login
        self.fp_log.write(f"{self.lp()} Sending login info....\n")

        with open(self.login_file) as f:
            dic = json.load(f)
            id = dic['id']
            pw = dic['pw']
        self.driver.find_element_by_id("userNameInput").send_keys(id)
        self.driver.find_element_by_id("passwordInput").send_keys(pw)
        time.sleep(1)
        self.driver.find_element_by_id("submitButton").click()
        time.sleep(3)
        self.fp_log.write(f"{self.lp()} Logged in!\n")

    def s30_query_course(self):
        # enter register page
        self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr/td[1]/button[5]").click()
        time.sleep(3)
        # query SFC520
        self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/table/tbody/tr[5]/td/table/tbody/tr[3]/td[1]/input").send_keys(self.course_code)
        time.sleep(1)
        self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/table/tbody/tr[5]/td/table/tbody/tr[3]/td[3]/a").click()
        time.sleep(3)
        self.fp_log.write(f"{self.lp()} {self.course_code} selected...\n")

    def s40_select_quarter(self):
        """Select which quarter to enroll."""

        # check this position carefully!
        idx = int(self.q - self.q_now)
        self.driver.find_element_by_xpath(f"/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/table/tbody/tr[3]/td/span[1]/table/tbody/tr/td/table/tbody/tr[{idx}]/td[1]/a").click()
        time.sleep(3)
        self.fp_log.write(f"{self.lp()} Quarter {self.q} selected...\n")

    def s50_check_available_groups(self):
        
        # check total number of groups
        n_groups = len(self.driver.find_elements_by_xpath("/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/span[3]/table/tbody/tr/td/table/tbody/tr")) - 1
        self.fp_log.write(f"{self.lp()} {n_groups} group(s) detected...\n")
        
        for i in range(1, n_groups + 1):
            # check each row
            if self.driver.find_element_by_xpath(f"/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/span[3]/table/tbody/tr/td/table/tbody/tr[{i+1}]/td[6]/span").text == "Full":
                self.fp_log.write(f"{self.lp()} Group {i} is full...\n")
            else:
                self.fp_log.write(f"{self.lp('INFO')} Group {i} is not full!\n")
                
                txt_time = self.driver.find_element_by_xpath(f"/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/span[3]/table/tbody/tr/td/table/tbody/tr[{i+1}]/td[13]/span").text
                self.fp_log.write(f"{self.lp()} Time description: {txt_time}\n")
                
                re_start = re.compile(r"""
                    (?P<hh>\d{1,2})  # starting hour
                    \:
                    (?P<mm>\d{2})  # starting minute
                    \-
                    \d{1,2}    # ending hour
                    \:
                    \d{2}      # ending minute
                    """, re.VERBOSE)
                hh = re_start.search(txt_time).group("hh")
                mm = re_start.search(txt_time).group("mm")
                self.dic_avail[i] = int(hh)  # hour is enough
                self.fp_log.write(f"{self.lp()} Group {i} begins on {hh}:{mm}\n")
                
    def s60_register_preferred(self):
        """Choose the most preferred slot if there are many."""
        
        # select action based on dic_avail
        n = len(self.dic_avail)
        
        # no registraion case
        if n == 0:
            self.fp_log.write(f"{self.lp()} All groups are full!\n")      
            return
        
        # list preferred group
        dic_preferred = {k: v for k, v in self.dic_avail.items() if v <= 15}  # no later than 15:45
        
        # a) pick the first preferred group if exist
        if len(dic_preferred) > 0:
            for k, _ in dic_preferred.items():
                i_register = k
                break
        # b) if no preferred group exists, pick the first group in the original dict
        else: 
            for k, _ in self.dic_avail.items():
                i_register = k
                break

        # select group in dropbown list
        select = Select(self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td[2]/table[1]/tbody/tr/td/table[2]/tbody/tr/td[5]/select"))
        select.select_by_value(str(i_register))
        self.fp_log.write(f"{self.lp()} Group {i_register} is selected....\n")                      
        time.sleep(1)

        # register
        self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table[7]/tbody/tr/td[2]/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td[3]/a").click()
        self.fp_log.write(f"{self.lp('INFO')} Registered to Group {i_register}!\n")                      
        time.sleep(3)
        # save state
        self.save_state("1")
        self.fp_log.write(f"{self.lp()} State = 1 saved!\n")                      
        return

    def s90_logout(self):
        # logout
        self.driver.find_element_by_xpath("/html/body/form/table/tbody/tr/td/table[1]/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr/td[2]/button").click()
        time.sleep(3)
        self.fp_log.write(f"{self.lp()} Logged out...\n")
        self.exit_program()


    def exit_program(self, code=0):
        """Close opened objects safely."""
        
        # close browser driver
        self.driver.close()
        
        # cleanup driver log
        if self.del_geckodriver:
            try:
                os.remove(self.base_dir / "geckodriver.log")
            except OSError as e:
                self.fp_log.write(f"{self.lp('ERROR')} Cannot delete geckodriver.log: {str(e)}")
        
        # cleanup program log
        self.fp_log.close()
        exit(code)

    def init_driver(self):
        """Open the backend browser."""
        options = Options()
        if self.hide_window:
            options.add_argument('--headless')
        return webdriver.Firefox(options=options)

    def load_state(self):
        if not self.state_file.is_file():
            return "0"
        else:
            with open(self.state_file) as f:
                st = f.read().strip()
            return st

    def save_state(self, state):
        with open(self.state_file, "w") as f:
            f.write(str(state))

    def lp(self, label="DEBUG"):
        """Log prefix"""
        return f"{datetime.now().strftime('%x %X')} [{label}]"


if __name__ == '__main__':

    o = OA()  # Set the parameters in __init__()
    o.master()
